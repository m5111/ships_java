package statki;

class Mast {

    private boolean hit;
    private Ship ship;

    Mast() {
        this.hit = false;
        this.ship = null;
    }

    final void hit() {
        hit = true;
    }

    final boolean isHit() {
        return hit;
    }

    final boolean isOwned() {
        return ship != null;
    }

    final Ship getOwner() {
        return ship;
    }

    final void setOwner(Ship ship) {
        this.ship = ship;
    }

    final String toString(boolean showShip) {
        if (isHit()) {
            if (isOwned()) {
                return "X";
            } else {
                return "+";
            }
        } else if (isOwned()) {
            if (showShip) {
                return "#";
            } else {
                return "_";
                //return "#";
            }
        } else {
            return "_";
        }
    }
}
