package statki;

import java.util.Scanner;

public class Human extends Player {

    Human(Game game, int id) {
        super(game, id);
    }

    @Override
    protected void onHit(HitResult result, Point p) {

    }

    private Boolean inputChecker(String input) {
        return input.length() == 2 &&
                Character.isLetter(input.charAt(0)) &&
                Character.isDigit(input.charAt(1)) &&
                input.charAt(0) <= 65 + Constants.boardSize - 1 &&
                input.charAt(0) >= 65 &&
                input.charAt(1) <= 48 + Constants.boardSize - 1 &&
                input.charAt(1) >= 48 &&
                (!notMyBoard[(int) input.charAt(0) - 65][(int) input.charAt(1) - 48].isHit());
    }

    @Override
    public final Point getXY() {
        String pole = "";
        Scanner scanner = new Scanner(System.in);
        while (!inputChecker(pole)) {
            pole = scanner.nextLine();
            pole = pole.toUpperCase();
            if (pole.length() != 2) {
                //noinspection SpellCheckingInspection
                System.out.println("Podaj 2 znaki!");
                continue;
            }

            if (!Character.isLetter(pole.charAt(0)) || !Character.isDigit(pole.charAt(1))) {
                //noinspection SpellCheckingInspection
                System.out.println("Podaj litere i cyfrę!");
                continue;
            }

            if ((int) pole.charAt(0) < 65 || (int) pole.charAt(0) > 65 + Constants.boardSize - 1) {
                //noinspection SpellCheckingInspection
                System.out.println("Podaj litere od A do J!");
                continue;
            }

            if ((int) pole.charAt(1) < 48 || (int) pole.charAt(1) > 48 + Constants.boardSize - 1) {
                //noinspection SpellCheckingInspection
                System.out.println("Podaj cyfre od 0 do 9");
                continue;
            }

            if (notMyBoard[(int) pole.charAt(0) - 65][(int) pole.charAt(1) - 48].isHit()) {
                //noinspection SpellCheckingInspection
                System.out.println("Juz wybrales to pole. Wybierz inne!");
            }
        }
        return new Point((int) pole.charAt(0) - 65, (int) pole.charAt(1) - 48);
    }

    @Override
    public final Direction getDirection() {
        Scanner scanner = new Scanner(System.in);
        //noinspection SpellCheckingInspection
        System.out.println("Podaj kierunek\n1.Gora\n2.Dol\n3.Lewo\n4.Prawo");
        int a = 0;
        while (a < 1 || a > 4) {
            a = Integer.parseInt(scanner.nextLine());
            if (a < 1 || a > 4) {
                //noinspection SpellCheckingInspection
                System.out.println("Podaj liczbe z zakresu od 1 do 4!");
            }
        }
        return Direction.values()[a-1];
    }

    @Override
    protected void setShips() {
        Main.playerSwitchScreen(geId());
        for (int i = 0; i < Constants.shipAmount; i++) {
            game.draw(myBoard, true);
            //noinspection SpellCheckingInspection
            System.out.println("Ustaw swój statek " + this.ships[i].getName());
            while (true) {
                if (setShip(i)) {
                    break;
                } else {
                    //noinspection SpellCheckingInspection
                    System.out.println("Nie Można ustawić statku w tym miejscu");
                }
            }
        }
    }
}
