package statki;

class Ship4Mast extends Ship {

    Ship4Mast(Mast[][] board) {
        //noinspection SpellCheckingInspection
        super(board, 4, "czteromasztowy");
    }
}
