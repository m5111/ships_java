package statki;

class Game {

    private final Mast[][][] board = new Mast[2][Constants.boardSize][Constants.boardSize];

    Game() {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < Constants.boardSize; j++) {
                for (int k = 0; k < Constants.boardSize; k++) {
                    board[i][j][k] = new Mast();
                }
            }
        }
    }

    final Mast[][] getBoard(int n) {
        return board[n];
    }

    final void draw(Mast[][] board, boolean showShips) {
        System.out.print("  ");
        for (int i = 0; i < Constants.boardSize; i++) {
            System.out.print(i);
            System.out.print(" ");
        }
        System.out.print("\n");
        for (int i = 0; i < Constants.boardSize; i++) {
            System.out.print((char) (i + 65));
            System.out.print("|");
            for (int j = 0; j < Constants.boardSize; j++) {
                System.out.print(board[i][j].toString(showShips));
                System.out.print("|");
            }
            System.out.println();
        }
    }
}
