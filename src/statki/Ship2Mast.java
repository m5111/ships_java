package statki;

class Ship2Mast extends Ship {

    Ship2Mast(Mast[][] board) {
        //noinspection SpellCheckingInspection
        super(board, 2, "dwumasztowy");
    }
}
