package statki;

class Point {

    final private int x;
    final private int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Point(Point p) {
        x = p.getX();
        y = p.getY();
    }

    final int getX() {
        return x;
    }

    final int getY() {
        return y;
    }
}
