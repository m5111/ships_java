package statki;

abstract class Player {

    final Ship[] ships = new Ship[Constants.shipAmount];
    final Game game;
    private final int id;
    Mast[][] notMyBoard;
    Mast[][] myBoard;

    Player(Game game, int id) {
        this.id = id;
        this.game = game;
        if (id == 0) {
            myBoard = game.getBoard(0);
            notMyBoard = game.getBoard(1);
        }
        if (id == 1) {
            myBoard = game.getBoard(1);
            notMyBoard = game.getBoard(0);
        }
        ships[0] = new Ship4Mast(myBoard);
        ships[1] = new Ship3Mast(myBoard);
        ships[2] = new Ship3Mast(myBoard);
        ships[3] = new Ship2Mast(myBoard);
        ships[4] = new Ship2Mast(myBoard);
        ships[5] = new Ship2Mast(myBoard);
        ships[6] = new Ship1Mast(myBoard);
        ships[7] = new Ship1Mast(myBoard);
        ships[8] = new Ship1Mast(myBoard);
    }

    final HitResult hit(Point p) {
        if (notMyBoard[p.getX()][p.getY()].isHit()) {
            throw new IllegalArgumentException("Point is already hit");
        }
        notMyBoard[p.getX()][p.getY()].hit();
        if (notMyBoard[p.getX()][p.getY()].isOwned()) {
            if (notMyBoard[p.getX()][p.getY()].getOwner().isSunk()) {
                onHit(HitResult.HIT_AND_SUNK, p);
                return HitResult.HIT_AND_SUNK;
            }
            onHit(HitResult.HIT, p);
            return HitResult.HIT;
        }
        onHit(HitResult.MISS, p);
        return HitResult.MISS;
    }

    protected abstract void onHit(HitResult result, Point p);

    protected abstract Point getXY();

    protected abstract Direction getDirection();

    final int geId() {
        return id;
    }

    final boolean didLose() {
        for (int i = 0; i < Constants.shipAmount; i++) {
            if (!ships[i].isSunk()) {
                return false;
            }
        }
        return true;
    }

    final Boolean setShip(int number) {
        Direction direction;
        boolean result = false;
        while (!result) {
            Point p = new Point(getXY());
            if (ships[number].getSize() == 1) {
                direction = Direction.UP;
            } else {
                direction = getDirection();
            }
            result = ships[number].set(p, direction);

            if (!result) {
                return false;
            }
        }
        return true;
    }

    protected abstract void setShips();
}
