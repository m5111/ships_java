package statki;

import java.util.Random;
import java.util.Stack;

public class Computer extends Player {

    private final Random random = new Random();
    private final Stack<Point> pointsToCheck;
    private final Stack<Point> nextHits;
    private final int[] hitsToHit;
    private Point lastHit;
    private Point secondHit;
    private states state;

    private enum states {
        RANDOM, SEEK, DESTROY
    }

    Computer(Game game, int id) {
        super(game, id);
        pointsToCheck = new Stack<>();
        nextHits = new Stack<>();
        state = states.RANDOM;
        hitsToHit = new int[]{-3, -2, -1, 1, 2, 3};
    }

    @Override
    protected void onHit(HitResult result, Point p) {
        if (result == HitResult.HIT) {
            if (state == states.SEEK) {
                pointsToCheck.clear();
                state = states.DESTROY;
                secondHit = p;
            } else if (state == states.RANDOM) {
                state = states.SEEK;
                pointsToCheck.addAll(getNeighbours(p));
                lastHit = p;
            }
        } else if (result == HitResult.HIT_AND_SUNK) {
            pointsToCheck.clear();
            nextHits.clear();
            state = states.RANDOM;
        }
    }

    private Stack<Point> getNeighbours(Point p) {
        Stack<Point> stack = new Stack<>();
        for (int y = -1; y <= 1; y++) {
            for (int x = -1; x <= 1; x++) {
                if (Math.abs(x) == Math.abs(y) ||
                        (p.getX() + x < 0 || p.getY() + y < 0 ||
                                p.getX() + x > Constants.boardSize - 1 ||
                                p.getY() + y > Constants.boardSize - 1) ||
                        notMyBoard[p.getX() + x][p.getY() + y].isHit()) {
                    continue;
                }
                stack.add(new Point(p.getX() + x, p.getY() + y));
            }
        }
        return stack;
    }

    private Point stateSeek() {
        if (!pointsToCheck.empty()) {
            return pointsToCheck.pop();
        } else {
            state = states.RANDOM;
            return stateRandom();
        }
    }

    private Point stateRandom() {
        int x;
        int y;
        do {
            x = random.nextInt(Constants.boardSize);
            y = random.nextInt(Constants.boardSize);
        } while (notMyBoard[x][y].isHit());
        return new Point(x, y);
    }

    @Override
    public final Point getXY() {
        Point p = new Point(0, 0);
//        System.out.println(state);
        if (state == states.RANDOM) {
            p = stateRandom();
        } else if (state == states.SEEK) {
            p = stateSeek();
        } else if (state == states.DESTROY) {
            p = stateDestroy();
        }
        System.out.print(Main.intToChar(p.getX()));
        System.out.println(p.getY());
        return p;
    }

    private Point stateDestroy() {
        if (nextHits.empty()) {
            if (lastHit.getX() == secondHit.getX()) {
                for (int i : hitsToHit) {
                    if (Main.checkPoint(secondHit.getX(), secondHit.getY() + i)) {
                        if (!notMyBoard[secondHit.getX()][secondHit.getY() + i].isHit() && secondHit.getY() + i != lastHit.getY())
                            nextHits.add(new Point(secondHit.getX(), secondHit.getY() + i));
                    }
                }
            } else if (lastHit.getY() == secondHit.getY()) {
                for (int i : hitsToHit) {
                    if (Main.checkPoint(secondHit.getX() + i, secondHit.getY())) {
                        if (!notMyBoard[secondHit.getX() + i][secondHit.getY()].isHit() && secondHit.getX() + i != lastHit.getX())
                            nextHits.add(new Point(secondHit.getX() + i, secondHit.getY()));
                    }
                }
            }
        }
        return nextHits.pop();
    }

    @Override
    public final Direction getDirection() {
        return Direction.values()[random.nextInt(4)];
    }

    @Override
    protected void setShips() {
        for (int i = 0; i < Constants.shipAmount; i++) {

            while (true) {
                if (setShip(i)) {
                    break;
                }
            }

        }
    }
}