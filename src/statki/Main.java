package statki;

import java.util.Scanner;

class Main {
    public static char intToChar(int a) {
        return (char) (a+65);
    }

    static boolean checkPoint(int x, int y) {
        return !(x < 0 || x > Constants.boardSize - 1 || y < 0 || y > Constants.boardSize - 1);
    }

    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private static void pause() {
        try {
            //noinspection ResultOfMethodCallIgnored
            System.in.read();
        } catch (Exception ignored) {
        }
    }

    private static void selectPlayer(Game game, Player[] player, int id, Scanner input) {
        //noinspection SpellCheckingInspection,SpellCheckingInspection,SpellCheckingInspection
        System.out.println("Gracz " + (id + 1) + " to?\n1. Człowiek\n2. Komputer");
        int a = 0;
        while (a < 1 || a > 2) {
            a = Integer.parseInt(input.nextLine());
            if (a < 1 || a > 2) {
                //noinspection SpellCheckingInspection
                System.out.println("Podaj liczbe z zakresu od 1 do 2!");
            }
            if (a == 1) {
                player[id] = new Human(game, id);
            }
            if (a == 2) {
                player[id] = new Computer(game, id);
            }
        }
    }

    private static void printLine() {
        System.out.println();
        for (int i = 0; i < Constants.boardSize * 2 + 2; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    private static void draw(Game game, Player player) {
        clearScreen();
        System.out.println("Player " + (player.geId() + 1));
        printLine();
        if (player.geId() == 0) {
            game.draw(game.getBoard(1), false);
        } else {
            game.draw(game.getBoard(0), false);
        }
        printLine();
        if (player.geId() == 0) {
            game.draw(game.getBoard(0), true);
        } else {
            game.draw(game.getBoard(1), true);
        }
        printLine();
    }

    private static boolean play(Game game, Player player, Player opponent) {
        boolean hit = true;
        while (hit && !opponent.didLose()) {
            hit = false;
            draw(game, player);
            //noinspection SpellCheckingInspection
            System.out.println("Podaj pole:");
            Point p = player.getXY();
            HitResult result = player.hit(p);
            draw(game, player);
            //noinspection SpellCheckingInspection
            System.out.println("Podaj pole:");
            System.out.print(intToChar(p.getX()));
            System.out.print(p.getY());
            if (result == HitResult.HIT) {
                //noinspection SpellCheckingInspection
                System.out.print("\nTrafiony!");
                hit = true;
            }
            if (result == HitResult.HIT_AND_SUNK) {
                //noinspection SpellCheckingInspection
                System.out.print("\nTrafiony! Zatopiony!");
                hit = true;
            }
            System.out.println();
            if (result == HitResult.MISS) {
                //noinspection SpellCheckingInspection
                System.out.println("\nPudlo!");

            }
            pause();
        }
        if (opponent.didLose()) {
            //noinspection SpellCheckingInspection
            System.out.println("Gracz " + (player.geId() + 1) + " Wygrywa!");
            return true;
        }
        clearScreen();
        playerSwitchScreen(opponent.geId());
        clearScreen();
        return false;
    }

    static void playerSwitchScreen(int id) {
        //noinspection SpellCheckingInspection
        System.out.println("Kolej gracza " + (id + 1));
        pause();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        clearScreen();
        Game game = new Game();
        Player[] player = new Player[2];
        selectPlayer(game, player, 0, scanner);
        selectPlayer(game, player, 1, scanner);

        player[0].setShips();
        player[1].setShips();

        int rounds = 0;

        while (true) {
            rounds++;
            if (play(game, player[0], player[1])) {
                break;
            }

            if (play(game, player[1], player[0])) {
                break;
            }
        }
        //noinspection SpellCheckingInspection
        System.out.println("Rundy: " + rounds);
        //noinspection SpellCheckingInspection
        System.out.println("Wcisnij ESC zeby wyjsc");
        String c = scanner.nextLine();
        while (c.length() == 0) {
            c = scanner.nextLine();
            if (!c.isEmpty()) {
                if (c.toCharArray()[0] != (char) 27)
                    //noinspection SpellCheckingInspection
                    break;
            }
            //noinspection SpellCheckingInspection
            System.out.println("Wcisnij ESC zeby wyjsc");
        }
    }
}
