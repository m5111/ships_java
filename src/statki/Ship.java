package statki;

class Ship {

    private final Mast[][] board;
    private final Mast[] mast;
    private final int size;
    private final String name;

    Ship(Mast[][] board, int size, String name) {
        this.board = board;
        this.size = size;
        this.name = name;
        mast = new Mast[size];
    }

    private int checkPlace(int x, int y) {
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 2; j++) {
                if (Main.checkPoint(i, j)) {
                    if (board[i][j].getOwner() != null && board[i][j].getOwner() != this) {
                        return 1;
                    }
                }
            }
        }
        return 0;
    }

    final boolean set(Point p, Direction direction) {
        int j = 0;
        switch (direction) {
            case UP:
                for (int i = p.getX(); i > p.getX() - size; i--) {
                    if (!Main.checkPoint(i, p.getY())) {
                        return false;
                    }
                }

                for (int i = p.getX(); i > p.getX() - size; i--) {
                    if (checkPlace(i, p.getY()) == 1) {
                        return false;
                    }
                }

                for (int i = p.getX(); i > p.getX() - size; i--) {
                    mast[j] = board[i][p.getY()];
                    mast[j].setOwner(this);
                    j++;
                }
                return true;

            case DOWN:
                for (int i = p.getX(); i < p.getX() + size; i++) {
                    if (!Main.checkPoint(i, p.getY())) {
                        return false;
                    }
                }

                for (int i = p.getX(); i < p.getX() + size; i++) {
                    if (checkPlace(i, p.getY()) == 1) {
                        return false;
                    }
                }

                for (int i = p.getX(); i < p.getX() + size; i++) {
                    mast[j] = board[i][p.getY()];
                    mast[j].setOwner(this);
                    j++;
                }
                return true;

            case LEFT:
                for (int i = p.getY(); i > p.getY() - size; i--) {
                    if (!Main.checkPoint(p.getX(), i)) {
                        return false;
                    }
                }

                for (int i = p.getY(); i > p.getY() - size; i--) {
                    if (checkPlace(p.getX(), i) == 1) {
                        return false;
                    }
                }

                for (int i = p.getY(); i > p.getY() - size; i--) {
                    mast[j] = board[p.getX()][i];
                    mast[j].setOwner(this);
                    j++;
                }
                return true;

            case RIGHT:
                for (int i = p.getY(); i < p.getY() + size; i++) {
                    if (!Main.checkPoint(i, p.getY())) {
                        return false;
                    }
                }

                for (int i = p.getY(); i < p.getY() + size; i++) {
                    if (checkPlace(p.getX(), i) == 1) {
                        return false;
                    }
                }

                for (int i = p.getY(); i < p.getY() + size; i++) {
                    mast[j] = board[p.getX()][i];
                    mast[j].setOwner(this);
                    j++;
                }
                return true;

            default:
                return false;
        }
    }

    final boolean isSunk() {
        for (int i = 0; i < size; i++) {
            if (!mast[i].isHit()) {
                return false;
            }
        }
        return true;
    }

    final int getSize() {
        return size;
    }

    final String getName() {
        return name;
    }
}
